module.exports = function (app, quotesCollection) {
    // Route for Quotes
    app.post('/quotes', async (req, res) => {
        try {
            const insertedQuotes = await quotesCollection.insertOne(req.body);
            res.status(201).json({
                success: true,
                error: null,
                data: insertedQuotes.insertedId,
            });
        } catch (error) {
            res.status(500).json({
                message: 'error to create quotes',
            });
            console.log('error when posting quote', error);
        }
    });
};
