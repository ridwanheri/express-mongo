const express = require('express');
const bodyParser = require('body-parser');
const app = express();
// setup mongodb
const MongoClient = require('mongodb').MongoClient;
const CONN_STRING = 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false';

const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('./swagger_output.json');

MongoClient.connect(CONN_STRING, (err, client) => {
    if (err) return console.error(err);
    console.log('Connected to Database');

    const db = client.db('babayaga');
    const quotesCollection = db.collection('quotes');

    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/', (req, res) => {
        res.sendFile(__dirname + '/index.html');
    });

    // setup for documentation using swagger
    app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));

    app.listen(3000, function () {
        console.log('listening on 3000');
    });

    require('./routes')(app, quotesCollection);
});
